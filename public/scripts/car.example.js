class Car {
    static list = [];

    static init(cars) {
        this.list = cars.map((i) => new this(i));
    }

    constructor({
        id,
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        available,
        type,
        year,
        options,
        specs,
        availableAt,
    }) {
        this.id = id;
        this.plate = plate;
        this.manufacture = manufacture;
        this.model = model;
        this.image = image;
        this.rentPerDay = rentPerDay;
        this.capacity = capacity;
        this.description = description;
        this.transmission = transmission;
        this.available = available;
        this.type = type;
        this.year = year;
        this.options = options;
        this.specs = specs;
        this.availableAt = availableAt;
    }

    render() {
        return `
        <div class="card p-3 mb-2 align-items-stretch" style="height:580px">
        <img src=${this.image} alt="" style="height:180px; object-fit: cover;">

        <div>
          <p class="font-weight-bold mt-4">${this.model} / ${this.type}</p>
        </div>
        <div>
          <h5 class="font-weight-bold">Rp. ${this.rentPerDay} / hari</h5>
        </div>
        <div>
          <p class="car-description" title="${this.description}">${this.description}</p>
        </div>
        <div class="m-1">
          <span><img src="./assets/fi_users.svg" class="mx-1"> ${this.capacity} Orang</span>
        </div>
        <div class="m-1">
          <span><img src="./assets/fi_settings.svg"  class="mx-1">${this.transmission}</span>
        </div>
        <div class="m-1">
          <span><img src="./assets/fi_calendar.svg"  class="mx-1">Tahun ${this.year}</span>
        </div>
        <button class="btn btn-success mt-3"> Pilih Mobil</button>
      </div>
    `;
    }
}