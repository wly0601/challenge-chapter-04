class App {
    constructor() {
        this.carContainerElement = document.getElementById("show-cars");
    }

    async init() {
        await this.load();
        this.run();
    }

    run = () => {
        // this.clear();
        Car.list.forEach((car) => {
            const node = document.createElement("div");
            node.className = "col-4 my-1";
            node.innerHTML = car.render();
            this.carContainerElement.appendChild(node);
        });
    };

    async load() {
        this.clear();
        let passenger = document.getElementById('number-of-occupant');
        let driver = document.getElementById('driver');
        let date = document.getElementById('date-select');
        let time = document.getElementById('take-car');

        //Extract Year, Month, and Day From User Input
        const dateArray = date.value.split("-");

        //Instance date Class for user input date
        const dateRent = new Date(dateArray[0], dateArray[1], dateArray[2], time.value, '00', '00', '00');

        const cars = await Binar.listCars();

        const availableCar = cars.filter((car) => {
            //Check all available cars
            const availableCheck = car.available;

            //Check all cars that is can be rent at specific time input by use. Assume that car.availableAt is the time when the car is ready to be rent.
            const readyToRent = car.availableAt < dateRent;

            //Check again that capacity of the car is greater than passenger. If yes, return true.
            const comparePersonAndCar = car.capacity > passenger.value;
            return availableCheck && comparePersonAndCar && readyToRent;
        })
        Car.init(availableCar);
    }

    clear = () => {
        let child = this.carContainerElement.firstElementChild;

        while (child) {
            child.remove();
            child = this.carContainerElement.firstElementChild;
        }
    };
}